﻿//#define CONSOLE_OUTPUT

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenCvSharp;

using arDroneOpenCv.Model;
using System.Threading;
using NativeWifi;
using System.Collections.ObjectModel;

namespace AutonomousFlightController
{
    public partial class FormMain : Form
    {
        #region Variables and constants
        
        //  Name of the AR-Drones network
        String ARDRONE_NETWORK_SSID = "ardrone_152085";

        //  Used for thread safe calls to update the HUI
        Thread statusUpdateThread;
        delegate void SetBatteryLevelCallback(int value);

        //  Hold references for the drone interface, image processor
        //  and flight controller
        private DroneVision droneVision;
        private ImageProcessor imageProcessor;
        private AutomatedController automatedController;

        #endregion Variables and constants

        #region InitialisationStatusUpdates

        /**
         *  Handles updates from the drone when connecting to the device
         */
        void droneVision_InitialisationStatusChange(object sender, EventArgs e)
        {
            #if CONSOLE_OUTPUT
            Console.WriteLine("Received update message");
            #endif

            progressBarConnection.PerformStep();

            //  Enable controls to allow the user to control the drone
            //  and enable the status group box
            if (droneVision.InitialisationStatus == DroneInitialisationStatus.COMPLETED)
            {
                #if CONSOLE_OUTPUT
                Console.WriteLine("Enabling controls");
                #endif

                groupBoxControl.Enabled = true;
                groupBoxStatus.Enabled = true;
                groupBoxCamera.Enabled = true;
                groupBoxFollowParameters.Enabled = true;
            }
        }

        #endregion InitialisationStatusUpdates

        #region Constructor and initialisation

        /**
         *  Constructor - creates the objects needed and intialise
         *  the form
         */
        public FormMain()
        {
            InitializeComponent();

            droneVision = new DroneVision();
            imageProcessor = new ImageProcessor();
            automatedController = new AutomatedController(droneVision);

            initialiseOpenCVCode();
            initialiseControls();
        }

        /**
         *  Create some interface elements for the open cv parameters
         */
        private void initialiseOpenCVCode()
        {
            Cv.NamedWindow("Control");
            Cv.CreateTrackbar("Low threshold", "Control", imageProcessor.LowThreshold, 255, OnLowThresholdChange);
            Cv.CreateTrackbar("High threshold", "Control", imageProcessor.HighThreshold, 255, OnHighThresholdChange);
            Cv.CreateTrackbar("Hue threshold", "Control", imageProcessor.HueThreshold, 5, OnHueThresholdChange);
        }

        /**
         *  Basically populates the dropdown list of allowable
         *  colours based upon the identified trackable colours
         */
        private void initialiseControls()
        {
            //  Iterate through the Enumeration to find possible colours to track
            String[] colours = Enum.GetNames(typeof(ColourTracking.ColourToTrack));

            //  Add them to the comboBox
            foreach (String s in colours)
            {
                comboBoxCardToFollow.Items.Add(s);
            }

            //  Select the relevant colour
            comboBoxCardToFollow.SelectedIndex = (int)imageProcessor.TrackingColour;
        }

        #endregion Constructor and initialisatiob

        #region TrackbarCallbacks

        private void OnLowThresholdChange(int value)
        {
            imageProcessor.LowThreshold = value;
        }

        private void OnHighThresholdChange(int value)
        {
            imageProcessor.HighThreshold = value;
        }

        private void OnHueThresholdChange(int value)
        {
            imageProcessor.HueThreshold = value;
        }

        #endregion TrackbarCallbacks

        #region UI functions

        /**
         *  Check the control PC is connected to the correct network
         */
        private Boolean isConnectedToCorrectWifiNetwork()
        {
            //  Built using code from:
            //  http://stackoverflow.com/questions/431755/get-ssid-of-the-wireless-network-i-am-connected-to-with-c-sharp-net-on-windows
            //  Accessed: 08-04-2013
            //
            //  Used to check the name of the network we are connected to
            Boolean result = false;
            WlanClient wlanClient = new WlanClient();

            //  Holds all of the SSID's the computer is currently connected to
            Collection<String> connectedSsids = new Collection<string>();

            //  Iterate through the connected networks
            foreach (WlanClient.WlanInterface wlanInterface in wlanClient.Interfaces)
            {
                Wlan.Dot11Ssid ssid = wlanInterface.CurrentConnection.wlanAssociationAttributes.dot11Ssid;
                connectedSsids.Add(new String(Encoding.ASCII.GetChars(ssid.SSID, 0, (int)ssid.SSIDLength)));
            }

            //  Look for the correct the identity string in the list
            if (connectedSsids.Contains(ARDRONE_NETWORK_SSID))
            {
                result = true;
            }

            return result;
        }

        /**
         *  Connect to the drone
         */
        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (isConnectedToCorrectWifiNetwork())
            {
                droneVision.InitialisationStatusChangeEvent += new EventHandler(droneVision_InitialisationStatusChange);
                
                droneVision.Initialise();
            }
            else
            {
                MessageBox.Show("You are not connected to the AR-Drone Wifi network");
            }
        }

        private void TakeOffClick(object sender, EventArgs e)
        {
            droneVision.TakeOff();
        }

        private void LandButtonClick(object sender, EventArgs e)
        {
            droneVision.Land();
        }

        private void StrafeLeftClick(object sender, EventArgs e)
        {
            droneVision.StrafeLeft();
        }

        private void StrafeRightClick(object sender, EventArgs e)
        {
            droneVision.StrafeRight();
        }

        private void StopButtonClick(object sender, EventArgs e)
        {
            droneVision.stop();
        }

        private void UpButtonClick(object sender, EventArgs e)
        {
            droneVision.Up();
        }

        private void DownButtonClick(object sender, EventArgs e)
        {
            droneVision.Down();
        }

        private void MoveForwardClick(object sender, EventArgs e)
        {
            droneVision.MoveForward();
        }

        private void MoveBackwardClick(object sender, EventArgs e)
        {
            droneVision.MoveBackward();
        }

        private void RotateClockwiseClick(object sender, EventArgs e)
        {
            droneVision.RotateClockwise();
        }

        private void RotateAntiClockwiseClick(object sender, EventArgs e)
        {
            droneVision.RotateAntiClockwise();
        }

        /**
         *  Set up events for the events triggered by the drone
         */
        private void Form1_Load(object sender, EventArgs e)
        {
            droneVision.NewFrameCaptured += new EventHandler(droneVision_NewFrameCaptured);
            droneVision.NewStatusUpdate += new EventHandler(droneVision_NewStatusUpdate);
        }

        /**
         *  Kill off the drone to allow the form to exit
         */
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            //droneVision.Land();
            try
            {
                if (droneVision != null)
                {
                    droneVision.Dispose();
                }
            }
            //  Drone vision tends to throw an exception when it has not
            //  connected to the drone and we try to exit
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        /**
         *  Allow the take off command to be overriden
         */
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            automatedController.DisableTakeoff = checkBoxDisableTakeOff.Checked;

            if (!checkBoxDisableTakeOff.Checked)
            {
                droneVision.Land();
            }
        }

#endregion UI functions

#region GeneralStatusUpdates

        /**
         *  Event raised by the AR-Drone object needs to call
         *  the UI update on a separate thread
         */
        void droneVision_NewStatusUpdate(object sender, EventArgs e)
        {
            this.statusUpdateThread = new Thread(new ThreadStart(this.updateStatusWindow));

            this.statusUpdateThread.Start();
        }

        /**
         *  Thread safe code for updating the status window
         */
        void updateStatusWindow()
        {
            setBatteryLevel(droneVision.Data.BatteryLevel);
        }

        /**
         *  Thread safe call to update the battery meter
         */
        void setBatteryLevel(int value)
        {
            if (this.progressBarBatteryLevel.InvokeRequired)
            {
                SetBatteryLevelCallback d = new SetBatteryLevelCallback(setBatteryLevel);

                try
                {
                    this.Invoke(d, new object[] { value });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            else
            {
                labelBatteryLevel.Text = "Battery: " + value.ToString();
                progressBarBatteryLevel.Value = value;
            }
        }

        #endregion GeneralStatusUpdates

        #region CameraUpdate

        /**
         *  Triggered when the drone registers a new frame is available
         */
        void droneVision_NewFrameCaptured(object sender, EventArgs e)
        {
            //Console.Out.WriteLine("New frame captured");
            try
            {
                if (droneVision.LastFrameCaptured != null)
                {
                    IplImage inputFrame = IplImage.FromBitmap(droneVision.LastFrameCaptured);

                    //pictureBox1.Image = droneVision.LastFrameCaptured;

                    //  Check that the image processor was able to execute
                    if (imageProcessor.processingLoop(inputFrame))
                    {
                        //  Get the contour highlighted image to display on the UI
                        IplImage processedIplImage = imageProcessor.HighlightedImage;

                        if (processedIplImage != null)
                        {
                            pictureBoxCamera.Image = processedIplImage.ToBitmap();
                        }

                        //  Now get the offset vector to use for processing
                        automatedController.controlLoop(imageProcessor.PercentageOfMatches,imageProcessor.OffsetVector,imageProcessor.ContourBoxArea);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                //  Ocassionally we will get an exception
                //  caused by the output image being locked
                Console.WriteLine(ex.ToString());
            }
        }

        #endregion CameraUpdate

        private void comboBoxCardToFollow_SelectedIndexChanged(object sender, EventArgs e)
        {
            imageProcessor.TrackingColour = (ColourTracking.ColourToTrack)comboBoxCardToFollow.SelectedIndex;

            #if CONSOLE_OUTPUT
            String cardColour = "";

            switch (imageProcessor.TrackingColour)
            {
                case ColourTracking.ColourToTrack.RED_CARD:
                    cardColour = "Red";
                    break;
                case ColourTracking.ColourToTrack.YELLOW_CARD:
                    cardColour = "Yellow";
                    break;
                case ColourTracking.ColourToTrack.GREEN_CARD:
                    cardColour = "Green";
                    break;
                case ColourTracking.ColourToTrack.BLUE_CARD:
                    cardColour = "Blue";
                    break;
                default:
                    break;
            }
            Console.Out.WriteLine("Tracking {0} card", cardColour);
            #endif
        }

    }
}
