﻿namespace AutonomousFlightController
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonStrafeLeft = new System.Windows.Forms.Button();
            this.buttonStrafeRight = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonUp = new System.Windows.Forms.Button();
            this.buttonDown = new System.Windows.Forms.Button();
            this.groupBoxConnect = new System.Windows.Forms.GroupBox();
            this.checkBoxDisableTakeOff = new System.Windows.Forms.CheckBox();
            this.progressBarConnection = new System.Windows.Forms.ProgressBar();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.groupBoxControl = new System.Windows.Forms.GroupBox();
            this.buttonClockwise = new System.Windows.Forms.Button();
            this.buttonAntiClockwise = new System.Windows.Forms.Button();
            this.buttonBackward = new System.Windows.Forms.Button();
            this.buttonForward = new System.Windows.Forms.Button();
            this.buttonLand = new System.Windows.Forms.Button();
            this.buttonTakeOff = new System.Windows.Forms.Button();
            this.groupBoxStatus = new System.Windows.Forms.GroupBox();
            this.labelBatteryLevel = new System.Windows.Forms.Label();
            this.progressBarBatteryLevel = new System.Windows.Forms.ProgressBar();
            this.groupBoxCamera = new System.Windows.Forms.GroupBox();
            this.pictureBoxCamera = new System.Windows.Forms.PictureBox();
            this.groupBoxFollowParameters = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxCardToFollow = new System.Windows.Forms.ComboBox();
            this.groupBoxConnect.SuspendLayout();
            this.groupBoxControl.SuspendLayout();
            this.groupBoxStatus.SuspendLayout();
            this.groupBoxCamera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCamera)).BeginInit();
            this.groupBoxFollowParameters.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonStrafeLeft
            // 
            this.buttonStrafeLeft.Location = new System.Drawing.Point(6, 174);
            this.buttonStrafeLeft.Name = "buttonStrafeLeft";
            this.buttonStrafeLeft.Size = new System.Drawing.Size(75, 23);
            this.buttonStrafeLeft.TabIndex = 6;
            this.buttonStrafeLeft.Text = "Strafe Left";
            this.buttonStrafeLeft.UseVisualStyleBackColor = true;
            this.buttonStrafeLeft.Click += new System.EventHandler(this.StrafeLeftClick);
            // 
            // buttonStrafeRight
            // 
            this.buttonStrafeRight.Location = new System.Drawing.Point(168, 174);
            this.buttonStrafeRight.Name = "buttonStrafeRight";
            this.buttonStrafeRight.Size = new System.Drawing.Size(75, 23);
            this.buttonStrafeRight.TabIndex = 7;
            this.buttonStrafeRight.Text = "Strafe Right";
            this.buttonStrafeRight.UseVisualStyleBackColor = true;
            this.buttonStrafeRight.Click += new System.EventHandler(this.StrafeRightClick);
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(87, 174);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 8;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.StopButtonClick);
            // 
            // buttonUp
            // 
            this.buttonUp.Location = new System.Drawing.Point(87, 145);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(75, 23);
            this.buttonUp.TabIndex = 9;
            this.buttonUp.Text = "Up";
            this.buttonUp.UseVisualStyleBackColor = true;
            this.buttonUp.Click += new System.EventHandler(this.UpButtonClick);
            // 
            // buttonDown
            // 
            this.buttonDown.Location = new System.Drawing.Point(87, 203);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(75, 23);
            this.buttonDown.TabIndex = 10;
            this.buttonDown.Text = "Down";
            this.buttonDown.UseVisualStyleBackColor = true;
            this.buttonDown.Click += new System.EventHandler(this.DownButtonClick);
            // 
            // groupBoxConnect
            // 
            this.groupBoxConnect.Controls.Add(this.checkBoxDisableTakeOff);
            this.groupBoxConnect.Controls.Add(this.progressBarConnection);
            this.groupBoxConnect.Controls.Add(this.buttonConnect);
            this.groupBoxConnect.Location = new System.Drawing.Point(353, 281);
            this.groupBoxConnect.Name = "groupBoxConnect";
            this.groupBoxConnect.Size = new System.Drawing.Size(252, 111);
            this.groupBoxConnect.TabIndex = 21;
            this.groupBoxConnect.TabStop = false;
            this.groupBoxConnect.Text = "Connection";
            // 
            // checkBoxDisableTakeOff
            // 
            this.checkBoxDisableTakeOff.AutoSize = true;
            this.checkBoxDisableTakeOff.Checked = true;
            this.checkBoxDisableTakeOff.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDisableTakeOff.Location = new System.Drawing.Point(117, 34);
            this.checkBoxDisableTakeOff.Name = "checkBoxDisableTakeOff";
            this.checkBoxDisableTakeOff.Size = new System.Drawing.Size(100, 17);
            this.checkBoxDisableTakeOff.TabIndex = 22;
            this.checkBoxDisableTakeOff.Text = "Disable take off";
            this.checkBoxDisableTakeOff.UseVisualStyleBackColor = true;
            this.checkBoxDisableTakeOff.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // progressBarConnection
            // 
            this.progressBarConnection.Location = new System.Drawing.Point(35, 74);
            this.progressBarConnection.Name = "progressBarConnection";
            this.progressBarConnection.Size = new System.Drawing.Size(182, 23);
            this.progressBarConnection.Step = 34;
            this.progressBarConnection.TabIndex = 21;
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(35, 29);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(75, 23);
            this.buttonConnect.TabIndex = 20;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // groupBoxControl
            // 
            this.groupBoxControl.Controls.Add(this.buttonClockwise);
            this.groupBoxControl.Controls.Add(this.buttonAntiClockwise);
            this.groupBoxControl.Controls.Add(this.buttonBackward);
            this.groupBoxControl.Controls.Add(this.buttonDown);
            this.groupBoxControl.Controls.Add(this.buttonForward);
            this.groupBoxControl.Controls.Add(this.buttonUp);
            this.groupBoxControl.Controls.Add(this.buttonStop);
            this.groupBoxControl.Controls.Add(this.buttonLand);
            this.groupBoxControl.Controls.Add(this.buttonStrafeRight);
            this.groupBoxControl.Controls.Add(this.buttonTakeOff);
            this.groupBoxControl.Controls.Add(this.buttonStrafeLeft);
            this.groupBoxControl.Enabled = false;
            this.groupBoxControl.Location = new System.Drawing.Point(353, 8);
            this.groupBoxControl.Name = "groupBoxControl";
            this.groupBoxControl.Size = new System.Drawing.Size(252, 267);
            this.groupBoxControl.TabIndex = 22;
            this.groupBoxControl.TabStop = false;
            this.groupBoxControl.Text = "Control";
            // 
            // buttonClockwise
            // 
            this.buttonClockwise.Location = new System.Drawing.Point(129, 48);
            this.buttonClockwise.Name = "buttonClockwise";
            this.buttonClockwise.Size = new System.Drawing.Size(84, 23);
            this.buttonClockwise.TabIndex = 22;
            this.buttonClockwise.Text = "Clockwise";
            this.buttonClockwise.UseVisualStyleBackColor = true;
            // 
            // buttonAntiClockwise
            // 
            this.buttonAntiClockwise.Location = new System.Drawing.Point(39, 48);
            this.buttonAntiClockwise.Name = "buttonAntiClockwise";
            this.buttonAntiClockwise.Size = new System.Drawing.Size(84, 23);
            this.buttonAntiClockwise.TabIndex = 21;
            this.buttonAntiClockwise.Text = "Anticlockwise";
            this.buttonAntiClockwise.UseVisualStyleBackColor = true;
            // 
            // buttonBackward
            // 
            this.buttonBackward.Location = new System.Drawing.Point(87, 77);
            this.buttonBackward.Name = "buttonBackward";
            this.buttonBackward.Size = new System.Drawing.Size(75, 23);
            this.buttonBackward.TabIndex = 20;
            this.buttonBackward.Text = "Backward";
            this.buttonBackward.UseVisualStyleBackColor = true;
            // 
            // buttonForward
            // 
            this.buttonForward.Location = new System.Drawing.Point(87, 19);
            this.buttonForward.Name = "buttonForward";
            this.buttonForward.Size = new System.Drawing.Size(75, 23);
            this.buttonForward.TabIndex = 19;
            this.buttonForward.Text = "Forward";
            this.buttonForward.UseVisualStyleBackColor = true;
            // 
            // buttonLand
            // 
            this.buttonLand.Location = new System.Drawing.Point(129, 109);
            this.buttonLand.Name = "buttonLand";
            this.buttonLand.Size = new System.Drawing.Size(75, 23);
            this.buttonLand.TabIndex = 6;
            this.buttonLand.Text = "Land";
            this.buttonLand.UseVisualStyleBackColor = true;
            // 
            // buttonTakeOff
            // 
            this.buttonTakeOff.Location = new System.Drawing.Point(48, 109);
            this.buttonTakeOff.Name = "buttonTakeOff";
            this.buttonTakeOff.Size = new System.Drawing.Size(75, 23);
            this.buttonTakeOff.TabIndex = 5;
            this.buttonTakeOff.Text = "Take off";
            this.buttonTakeOff.UseVisualStyleBackColor = true;
            // 
            // groupBoxStatus
            // 
            this.groupBoxStatus.Controls.Add(this.labelBatteryLevel);
            this.groupBoxStatus.Controls.Add(this.progressBarBatteryLevel);
            this.groupBoxStatus.Enabled = false;
            this.groupBoxStatus.Location = new System.Drawing.Point(10, 281);
            this.groupBoxStatus.Name = "groupBoxStatus";
            this.groupBoxStatus.Size = new System.Drawing.Size(128, 111);
            this.groupBoxStatus.TabIndex = 25;
            this.groupBoxStatus.TabStop = false;
            this.groupBoxStatus.Text = "Status";
            // 
            // labelBatteryLevel
            // 
            this.labelBatteryLevel.Location = new System.Drawing.Point(16, 55);
            this.labelBatteryLevel.Name = "labelBatteryLevel";
            this.labelBatteryLevel.Size = new System.Drawing.Size(100, 16);
            this.labelBatteryLevel.TabIndex = 1;
            this.labelBatteryLevel.Text = "Battery";
            // 
            // progressBarBatteryLevel
            // 
            this.progressBarBatteryLevel.Location = new System.Drawing.Point(16, 74);
            this.progressBarBatteryLevel.Name = "progressBarBatteryLevel";
            this.progressBarBatteryLevel.Size = new System.Drawing.Size(100, 23);
            this.progressBarBatteryLevel.TabIndex = 0;
            // 
            // groupBoxCamera
            // 
            this.groupBoxCamera.Controls.Add(this.pictureBoxCamera);
            this.groupBoxCamera.Enabled = false;
            this.groupBoxCamera.Location = new System.Drawing.Point(10, 8);
            this.groupBoxCamera.Name = "groupBoxCamera";
            this.groupBoxCamera.Size = new System.Drawing.Size(337, 267);
            this.groupBoxCamera.TabIndex = 26;
            this.groupBoxCamera.TabStop = false;
            this.groupBoxCamera.Text = "Camera";
            // 
            // pictureBoxCamera
            // 
            this.pictureBoxCamera.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxCamera.Location = new System.Drawing.Point(8, 19);
            this.pictureBoxCamera.Name = "pictureBoxCamera";
            this.pictureBoxCamera.Size = new System.Drawing.Size(320, 240);
            this.pictureBoxCamera.TabIndex = 24;
            this.pictureBoxCamera.TabStop = false;
            // 
            // groupBoxFollowParameters
            // 
            this.groupBoxFollowParameters.Controls.Add(this.comboBoxCardToFollow);
            this.groupBoxFollowParameters.Controls.Add(this.label1);
            this.groupBoxFollowParameters.Enabled = false;
            this.groupBoxFollowParameters.Location = new System.Drawing.Point(144, 281);
            this.groupBoxFollowParameters.Name = "groupBoxFollowParameters";
            this.groupBoxFollowParameters.Size = new System.Drawing.Size(203, 111);
            this.groupBoxFollowParameters.TabIndex = 27;
            this.groupBoxFollowParameters.TabStop = false;
            this.groupBoxFollowParameters.Text = "Follow parameters";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(23, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Card colour to follow:";
            // 
            // comboBoxCardToFollow
            // 
            this.comboBoxCardToFollow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCardToFollow.FormattingEnabled = true;
            this.comboBoxCardToFollow.Location = new System.Drawing.Point(23, 74);
            this.comboBoxCardToFollow.Name = "comboBoxCardToFollow";
            this.comboBoxCardToFollow.Size = new System.Drawing.Size(156, 21);
            this.comboBoxCardToFollow.TabIndex = 2;
            this.comboBoxCardToFollow.SelectedIndexChanged += new System.EventHandler(this.comboBoxCardToFollow_SelectedIndexChanged);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 402);
            this.Controls.Add(this.groupBoxFollowParameters);
            this.Controls.Add(this.groupBoxCamera);
            this.Controls.Add(this.groupBoxStatus);
            this.Controls.Add(this.groupBoxControl);
            this.Controls.Add(this.groupBoxConnect);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBoxConnect.ResumeLayout(false);
            this.groupBoxConnect.PerformLayout();
            this.groupBoxControl.ResumeLayout(false);
            this.groupBoxStatus.ResumeLayout(false);
            this.groupBoxCamera.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCamera)).EndInit();
            this.groupBoxFollowParameters.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonStrafeLeft;
        private System.Windows.Forms.Button buttonStrafeRight;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.GroupBox groupBoxConnect;
        private System.Windows.Forms.ProgressBar progressBarConnection;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.GroupBox groupBoxControl;
        private System.Windows.Forms.Button buttonClockwise;
        private System.Windows.Forms.Button buttonAntiClockwise;
        private System.Windows.Forms.Button buttonBackward;
        private System.Windows.Forms.Button buttonForward;
        private System.Windows.Forms.Button buttonLand;
        private System.Windows.Forms.Button buttonTakeOff;
        private System.Windows.Forms.GroupBox groupBoxStatus;
        private System.Windows.Forms.Label labelBatteryLevel;
        private System.Windows.Forms.ProgressBar progressBarBatteryLevel;
        private System.Windows.Forms.GroupBox groupBoxCamera;
        private System.Windows.Forms.PictureBox pictureBoxCamera;
        private System.Windows.Forms.CheckBox checkBoxDisableTakeOff;
        private System.Windows.Forms.GroupBox groupBoxFollowParameters;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxCardToFollow;
    }
}

