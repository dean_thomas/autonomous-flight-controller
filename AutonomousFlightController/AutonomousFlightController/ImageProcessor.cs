﻿//#define outputDebugImages
//#define CONSOLE_OUTPUT

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenCvSharp;
using System.Drawing;

/**
 *  @class ImageProcessor
 *  @author Dean Thomas
 *  @email dean.thomas@postgrad.plymouth.ac.uk
 *  @date 11-04-2013
 *  
 *  This class carries out the image processing required to track a
 *  specified coloured card
 */
namespace AutonomousFlightController
{
    class ImageProcessor
    {
        #region Constants

        //  Contours with dimensions smaller than this 
        //  will be classified as noise
        public const int MIMIMUM_CONTOUR_SIZE = 50;

        #endregion Constants

        #region Variables

        //  contourBoxArea gives some rough estimates on the distance to the target
        private int contourBoxArea;
        public int ContourBoxArea { get { return contourBoxArea;}}

        //  offsetVector holds information relating to how much up/down/left/right
        //  movement is required to centre the contour in view
        private Size offsetVector;
        public Size OffsetVector { get { return offsetVector; } }

        //  highlightedImage holds a represenation of the input with tracked
        //  contours highlighted
        private IplImage highlightedImage;
        public IplImage HighlightedImage { get { return highlightedImage; } }

        //  Variables for the thresholding algorithms
        private int lowThreshold = 100;
        public int LowThreshold { get { return lowThreshold; } set { lowThreshold = value; } }
        private int highThreshold = 200;
        public int HighThreshold { get { return highThreshold; } set { highThreshold = value; } }

        //  Amount of variation either side of the target hue
        private int hueThreshold = 3;
        public int HueThreshold { get { return hueThreshold; } set { hueThreshold = value; } }

        //  Keep a reference of the coloured card to track
        private ColourTracking.ColourToTrack trackingColour;
        public ColourTracking.ColourToTrack TrackingColour { get { return trackingColour; } set { trackingColour = value; } }       

        //  Number of frames processed in the current sample
        int totalFramesProcessed = 0;
        public int TotalFrameProcessed { get { return totalFramesProcessed; } }

        //  Number of frames containing the target object in the current sample
        int validMatches = 0;
        public int ValidMatches { get { return validMatches; } }

        //  The number of valid matches in the current sample as a percentage
        private float percentageOfMatches = 0;
        public float PercentageOfMatches { get { return percentageOfMatches; } }
        
        //  Used to keep track of the sample timeframe
        static DateTime REFERENCE_DATE = new DateTime(1970, 1, 1);
        DateTime sampleStart = REFERENCE_DATE;

        //  How often the sample window should refresh
        private int millisecondsBetweenSamples = 2500;
        public int MillisecondsBetweenSamples { get { return millisecondsBetweenSamples; } set { millisecondsBetweenSamples = value; } }

        //  Flag to keep track of when processing is tacking place
        private Boolean isBusy = false;
        public Boolean IsBusy { get { return isBusy; } }

        #endregion Variables

        #region Constructor

        public ImageProcessor()
        {
            this.isBusy = false;
            this.trackingColour = ColourTracking.ColourToTrack.RED_CARD;
        }

        #endregion Constructor

        #region ListFunctions

        /**
         *  Turn the C style basic list of contours to something a bit more
         *  user friendly
         */
        private List<CvSeq<CvPoint>> createContourList(CvSeq<CvPoint> contoursSeq)
        {
            List<CvSeq<CvPoint>> result = new List<CvSeq<CvPoint>>();

            //  Hold the contour currently being processed
            CvSeq<CvPoint> currentContour;

            //  Iterate through each contour
            for (currentContour = contoursSeq; currentContour != null; currentContour = currentContour.HNext)
            {
                //  Add to the list
                result.Add(currentContour);
            }
            return result;
        }

        /**
         *  Remove contours that appear to be noise in the list using the specified
         *  minimum size
         */
        public List<CvSeq<CvPoint>> filterList(List<CvSeq<CvPoint>> contoursList,int minSizeInPixels)
        {
            List<CvSeq<CvPoint>> result = new List<CvSeq<CvPoint>>();

            //  Loop through the supplied list
            for (int i = 0; i < contoursList.Count; i++)
            {
                CvRect boundingRect = Cv.BoundingRect(contoursList[i]);

                //  If the dimensions in both direction are above the threshold
                //  then we will not discard it as noise
                if ((boundingRect.Width > minSizeInPixels) && (boundingRect.Height > minSizeInPixels))
                {
                    result.Add(contoursList[i]);
                }
            }
            return result;
        }

        #endregion ListFunctions

        #region Static functions
        
        /**
         *  Simply return the bounding rectangle of the supplied contour
         */
        static public CvRect boundingRectOfContour(CvSeq<CvPoint> contour)
        {
            CvRect result = Cv.BoundingRect(contour);

            return result;
        }

        /**
         *  Take the supplied RGB image and convert it to HSV colour space
         */
        static public IplImage covertToHSV(IplImage inputFrame)
        {
            IplImage result = new IplImage(inputFrame.Size, inputFrame.Depth, 3);

            Cv.CvtColor(inputFrame, result, ColorConversion.RgbToHsv);

            return result;
        }

        /**
         *  Split the supplied HSV image into separate Hue, Saturation and Value
         *  images
         */
        static public void splitHSV(IplImage hsvImage, 
            out IplImage hueImage, 
            out IplImage saturationImage,
            out IplImage valueImage)
        {
            //  Holders for the resultant images
            hueImage = new IplImage(hsvImage.Size, hsvImage.Depth, 1);
            saturationImage = new IplImage(hsvImage.Size, hsvImage.Depth, 1);
            valueImage = new IplImage(hsvImage.Size, hsvImage.Depth, 1);

            //  Do the actual split
            Cv.Split(hsvImage, hueImage, saturationImage, valueImage, null);
        }

        /**
         *  Create a new image in which only the specified hue is highlighted.
         *  Includes a tolerance on the hue of a small amount in either direction
         */
        static public IplImage highlightHue(IplImage input, int hue, int tolerance)
        {
            IplImage result = new IplImage(input.Size, input.Depth, 1);

            //  Find how many pixels are in the image
            int a = input.ImageSize;

            //  Loop through each pixel serially
            for (int i = 0; i < input.ImageSize; i++)
            {
                if (((input.Get1D(i).Val0 >= (hue - tolerance)) && (input.Get1D(i).Val0 <= (hue + tolerance))))
                {
                    //  Set matching (or good enough) values to white  
                    result.Set1D(i, new CvScalar(255));
                }
                else
                {
                    //  Non-matching pixels are marked black
                    result.Set1D(i, new CvScalar(0));
                }

            }
            return result;
        }

        #endregion STATIC_METHODS

        #region Main Processing function

        /**
         *  Do the actual processing of the image by working with the supplied frame
         *  
         *  @return true if processing was sucessfull
         */
        public Boolean processingLoop(IplImage frame)
        {
            //  Return if processing was successful
            Boolean result = false;

            //  Used to hold the highlighted contours whilst processing
            IplImage tempImage;

            //  Check we have a frame supllied and we aren't already processing
            //  anything else
            if ((frame != null) && (!isBusy))
            {
                isBusy = true;

                tempImage = Cv.Clone(frame);

                #if outputDebugImages
                Cv.ShowImage("Input", frame);
                #endif

                //  Convert the image to HSV so that we can track hues
                IplImage hsvImage = ImageProcessor.covertToHSV(frame);

                //  Remove noise
                IplImage blurredHSVImage = new IplImage(frame.Size, frame.Depth, 3);
                Cv.Smooth(hsvImage, blurredHSVImage, SmoothType.Gaussian, 5, 5);

                //  Holders for individual channels
                IplImage hueImage = new IplImage(frame.Size, frame.Depth, 1);
                IplImage saturationImage = new IplImage(frame.Size, frame.Depth, 1);
                IplImage valueImage = new IplImage(frame.Size, frame.Depth, 1);

                //  Split in to separate images for Hue, Saturation and Value
                ImageProcessor.splitHSV(blurredHSVImage, out hueImage, out saturationImage, out valueImage);

                //BitmapConverter.DrawToGraphics(hueImage, pictureBox1.CreateGraphics(), boundingRect);
                #if outputDebugImages
                Cv.ShowImage("Hue Image", hueImage);
                #endif

                #if CONSOLE_OUTPUT
                outputHueToConsole(hueImage);
                #endif

                //  Hue to be tracked
                int trackingHue = ColourTracking.hueValueForTrackingColour(trackingColour);

                //  Create an image where only the desired hue is marked
                IplImage hueHighlightedImage = ImageProcessor.highlightHue(hueImage, trackingHue, hueThreshold);
                #if outputDebugImages
                Cv.ShowImage("Highlight hue", hueHighlightedImage);
                #endif

                //  Find contours in the highlighted image
                CvMemStorage contoursMemStorage = Cv.CreateMemStorage();
                CvSeq<CvPoint> contoursSeq;
                Cv.FindContours(hueHighlightedImage, contoursMemStorage, out contoursSeq);

                //  Store the contours in a list structure and remove noise
                List<CvSeq<CvPoint>> contourList = filterList(createContourList(contoursSeq), MIMIMUM_CONTOUR_SIZE);
                #if CONSOLE_OUTPUT
                    Console.WriteLine("Contours " + contourList.Count.ToString());
                #endif

                //  Draw on a copy of the input
                if (contourList.Count > 0)
                {
                    drawContour(tempImage, contourList[0]);

                    CvRect rect = ImageProcessor.boundingRectOfContour(contourList[0]);

                    Point centrePoint = new Point(rect.Left + (rect.Width / 2), rect.Top + (rect.Height / 2));

                    //  Update the offest vector as the centre of the frame to
                    //  the centre of the contour
                    offsetVector = calculateOffset(frame, centrePoint);

                    //  Update the (rough) contour area as the width * height
                    //  this should allow a rough estimation of distance
                    contourBoxArea = rect.Width * rect.Height;
                }

                //  Keep track if the object has been seen in this frame
                incrementMatchCounters(contourList.Count > 0);

                //  Make the highlighted image available elsewhere
                this.highlightedImage = tempImage;

                //  sucessful conclusion
                result = true;

                //  Reset the flag so that another frame can be processed
                isBusy = false;
            }
            return result;
        }

        #endregion Main Processing function

        #region Statistical functions

        /**
         *  Calculate the vector need to move the contour to the centre of the screen
         */
        private Size calculateOffset(IplImage window, Point centreOfContour)
        {
            Size result = new Size();

            result = new Size((window.Size.Width / 2) - centreOfContour.X, (window.Size.Height / 2) - centreOfContour.Y);

            #if CONSOLE_OUTPUT
            //Console.Out.WriteLine("Offset: {0}", result);
            #endif

            return result;
        }

        /**
         *  Update the number of frames processed and the number of frames
         *  where the card has been recognised
         */
        private void incrementMatchCounters(Boolean sawObject)
        {

            //  Start a new sample
            if ((sampleStart == REFERENCE_DATE) || (DateTime.Now > sampleStart.AddMilliseconds(millisecondsBetweenSamples)))
            {
                sampleStart = DateTime.Now;
                totalFramesProcessed = 0;
                validMatches = 0;

                #if CONSOLE_OUTPUT
                Console.WriteLine("Created new sample");
                Console.WriteLine("{0}.{1}", sampleStart, sampleStart.Millisecond);
                #endif
            }

            //  Object was seen in the last frame
            if (sawObject)
            {
                validMatches++;
            }

            totalFramesProcessed++;

            //  Work out the percentage of valid matches
            percentageOfMatches = (float)validMatches / (float)totalFramesProcessed;
            
            #if CONSOLE_OUTPUT
            if ((validMatches > 0) && (totalFramesProcessed > 0))
            {
                Console.WriteLine("Processed frames: {0} Matching frames: {1} Percentage: {2}", totalFramesProcessed, validMatches, percentageOfMatches);
            }
            #endif
        }

        #endregion Statistical Functions

        #region Debugging functions

        /**
         *  Draw the specified contour on the supplied image
         */
        private void drawContour(IplImage image, CvSeq<CvPoint> contour)
        {
            CvScalar DRAWING_COLOUR = new CvScalar(0, 255, 0);

            CvRect rectangle = Cv.BoundingRect(contour);

            //  Draw the contour
            Cv.DrawContours(image, contour, DRAWING_COLOUR, DRAWING_COLOUR, -1);

            //  drawing the bounding box
            Cv.DrawRect(image, rectangle, DRAWING_COLOUR);

            //  draw the centre mark
            Cv.DrawCircle(image, new CvPoint(rectangle.Left + (rectangle.Width / 2), rectangle.Top + (rectangle.Height / 2)), 3, new CvScalar(255, 0, 0));
        }

        /**
         *  Output the hue value of the pixel at the centre of the image
         */
        private void outputHueToConsole(IplImage hueImage)
        {
            Console.WriteLine(hueImage.Get2D(hueImage.Width / 2, hueImage.Height / 2).Val0.ToString() +
                        "," + hueImage.Get2D(hueImage.Width / 2, hueImage.Height / 2).Val1.ToString() +
                        "," + hueImage.Get2D(hueImage.Width / 2, hueImage.Height / 2).Val2.ToString());
        }

        #endregion Debug Functions
    }
}
