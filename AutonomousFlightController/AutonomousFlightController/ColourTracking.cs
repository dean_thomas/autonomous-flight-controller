﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutonomousFlightController
{
    public static class ColourTracking
    {
        /*
         *  Card Hues as measured from the drone camera
         */
        public const int RED_CARD_HUE       = 118;
        public const int GREEN_CARD_HUE     = 48;
        public const int YELLOW_CARD_HUE    = 98;
        public const int BLUE_CARD_HUE      = 25;

        /*
         *  Define a set of possible values
         */
        public enum ColourToTrack
        {
            BLUE_CARD                       = 0x00,
            RED_CARD                        = 0x01,
            YELLOW_CARD                     = 0x02,
            GREEN_CARD                      = 0x03
        }

        /*
         *  Given the target enumeration return the pre-measured hue based on
         *  the coloured cards
         */
        public static int hueValueForTrackingColour(ColourToTrack trackingColour)
        {
            switch (trackingColour)
            {
                case ColourTracking.ColourToTrack.BLUE_CARD:
                    return ColourTracking.BLUE_CARD_HUE;
                case ColourTracking.ColourToTrack.RED_CARD:
                    return ColourTracking.RED_CARD_HUE;
                case ColourTracking.ColourToTrack.YELLOW_CARD:
                    return ColourTracking.YELLOW_CARD_HUE;
                case ColourTracking.ColourToTrack.GREEN_CARD:
                    return ColourTracking.GREEN_CARD_HUE;
                default:
                    throw new NotImplementedException("Invalid tracking colour");
            }
        }
    }
}
