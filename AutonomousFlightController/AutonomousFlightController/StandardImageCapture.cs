﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenCvSharp;
using System.Drawing;


/**
 *  No longer used in the project
 */
namespace AutonomousFlightController
{
    class StandardImageCapture
    {
        CvCapture capture;

        public Size getFrameSize()
        {
            Size result = new Size(0,0);

            if (capture != null)
            {
                result = new Size(capture.FrameWidth, capture.FrameHeight);
            }

            return result;
        }

        public StandardImageCapture()
        {
            try
            {
                capture = Cv.CreateCameraCapture(CaptureDevice.Any);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public IplImage getFrame()
        {
            IplImage result = null;

            if (capture != null)
            {
                result = capture.QueryFrame();
            }   
            return result;
        }
    }
}
