﻿//#define USE_ALVAR
//#define USE_AUTOMATION
//#define CONSOLE_OUTPUT
//#define USE_WPF

using ARDrone.Capture;
using ARDrone.Control;
using ARDrone.Control.Data;
using ARDrone.Control.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Media.Media3D;
using System.Threading;
using ARDrone.Control.Commands;

/*
 * Gareth Williams
 * 
 * will become an API for interfacing onto the drones camera and processed image data
 * 
 * 18/01/2013 -
 * extracted the alvar wrapper and bridge from Goblin XNA 4.1 using various references (including Tutorial8)
 * and mashed with the camera feed from the ar drone WPF sample
 * 
 * some useful references:
 * The source code for AlvarWrapper.dll in GoblinXNAx.x/wrappers/alvar2.0 (how the calls wrap alvar)
 * The source for Scene.cs, MarkerNode.cs, AlvarMarker.cs and NullCapture.GetTexture() (how Goblin uses the alvarDLLBridge / default values)
 * OpenCV IPLImage structure:
 * http://opencv.willowgarage.com/documentation/basic_structures.html
 * (AlvarWrapper injects parameters into this structure to use with Alvar200.dll)
 * IntPtr, Marshall, MemoryStream in the .net System namespace (these are managed pointer malloc and stream types used in interop code by AlvarBridge)
 * P/Invoke (this is how alvarDLLBridge talks to the AlvarWrapper.dll)
 * 
 * AR Drone capture example for hardcoded video values:
 * 
 * I would also read up on threading in .net, mainly where it applies to events,
 * this book is useful:
 * http://www.amazon.com/Programming-NET-Components-Juval-Lowy/dp/0596102070
 * 
 * It is possible to connect more than one device and client to the ar-drones server so 
 * for safety it makes sense to connect a mobile device with the free-flight app for 
 * manual override whilst developing.
 * 
 * todo:
 * Your first task if you use this sample should be to refactor the code ;)  
 * Your second task should be to expand the command and feedback since the api has poor handling of emergency states
 * 
 */


namespace arDroneOpenCv.Model
{
    public enum DroneInitialisationStatus
    {
        ERROR           = 0x00,
        NO_CONNECTION   = 0x01,
        BOOTSTRAPPING   = 0x02,
        CONNECTING      = 0x03,
        COMPLETED       = 0x04
    }

    public class DroneVision : IDisposable
    {
        //  Status data
        DroneData data;

        //  Accessor for status data
        public DroneData Data { get { return data; } }

        private System.Drawing.Bitmap m_lastFrameCaptured;
        public System.Drawing.Bitmap LastFrameCaptured
        {
            get { return m_lastFrameCaptured; }
        }

        ~DroneVision()
        {
            
                /*
                 * TimerCallback videoUpdateTimerCallback = this.getFrame;
            TimerCallback statusUpdateTimerCallback = this.getStatusUpdate;
            
            timerVideoUpdate = new Timer(videoUpdateTimerCallback,null,0,VIDEO_UPDATE_RATE);
            timerStatusUpdate*/
        }
        
        private DroneInitialisationStatus initialisationStatus;

        private void updateInitialisationStatus(DroneInitialisationStatus newInitialisationStatus)
        {
#if CONSOLE_OUTPUT
            Console.WriteLine("Old connection status: " + initialisationStatus.ToString()
                + " New connection status: " + newInitialisationStatus.ToString());
#endif
            if (newInitialisationStatus != initialisationStatus)
            {
                this.initialisationStatus = newInitialisationStatus;

                if (InitialisationStatusChangeEvent != null)
                {
                    InitialisationStatusChangeEvent(this, EventArgs.Empty);
                }
            }
        }

        public DroneInitialisationStatus InitialisationStatus { get { return initialisationStatus; } }

        public event EventHandler InitialisationStatusChangeEvent;
        
        public event EventHandler NewFrameCaptured;
        public event EventHandler NewStatusUpdate;

        #region alvar
#if USE_ALVAR
        int m_alvarCameraID;
        int m_alvarDetectorID;

        //for transformations
        int[] multiIDs;
        double[] multiPoseMats;
        double[] multiErrors;
        IntPtr multiIdPtr;
        IntPtr multiPosePtr;
        //IntPtr multiHideTexturePtr;
        IntPtr multiErrorPtr;
        List<string> multiMarkerIDs;

        //Matrix3D camProjMat;

        //for automation
        bool m_markerInRange = false;
        double m_yAxisAngleToMarkerDeg;
        double m_xAxisAngleToMarkerDeg;
        double m_distanceToMarker;
#endif
        #endregion

        #region ardrone

        //flags for cross thread callbacks
        bool m_isDroneBootstrapComplete = false;
        bool m_isDroneConnectionComplete = false;
        
        delegate void OutputEventHandler(String output);

        const int VIDEO_UPDATE_RATE = 50; //ms
        const int STATUS_UPDATE_RATE = 50; //ms

        System.Threading.Timer timerStatusUpdate;
        System.Threading.Timer timerVideoUpdate;

        DroneControl droneControl;
        DroneConfig currentDroneConfig;

#if USE_AUTOMATION
        BackgroundWorker automateDrone;
#endif

        #endregion

        #region API

        public DroneVision()
        {
            updateInitialisationStatus(DroneInitialisationStatus.NO_CONNECTION);
        }

        public void Initialise()
        {
            InitializeDroneControl();
            droneControl.NetworkConnectionStateChanged += new DroneNetworkConnectionStateChangedEventHandler(droneControl_NetworkConnectionStateChanged);
            droneControl.ConnectToDroneNetwork();

            //marshall using flag
            while (false == m_isDroneBootstrapComplete)
            {
                updateInitialisationStatus(DroneInitialisationStatus.BOOTSTRAPPING);
#if CONSOLE_OUTPUT
                Debug.WriteLine("waiting for bootstrap to complete");
#endif
            }

            InitializeDroneControlEventHandlers();
            Connect();

            //marshall using flag
            while (false == m_isDroneConnectionComplete)
            {
                updateInitialisationStatus(DroneInitialisationStatus.CONNECTING);
#if CONSOLE_OUTPUT
                Debug.WriteLine("waiting for connection complete");
#endif
            }

            updateInitialisationStatus(DroneInitialisationStatus.COMPLETED);

            InitializeTimers();
            
            //lift off :)
            //Command takeoff = new FlightModeCommand(DroneFlightMode.TakeOff);
            //Debug.Assert(droneControl.IsCommandPossible(takeoff), "drone is not ready for take off");
            //droneControl.SendCommand(takeoff);

            //image process entry point
            //timerVideoUpdate.Start();
            //drone automation entry point
            //timerStatusUpdate.Start();
            
            //adjust to marker card
#if USE_AUTOMATION
            automateDrone = new BackgroundWorker();
            automateDrone.DoWork += new DoWorkEventHandler(automateDrone_DoWork);
            automateDrone.RunWorkerCompleted += new RunWorkerCompletedEventHandler(automateDrone_RunWorkerCompleted);
            automateDrone.RunWorkerAsync();
#endif
        }

        void automateDrone_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Command land = new FlightModeCommand(DroneFlightMode.Land);
            Debug.Assert(droneControl.IsCommandPossible(land), "drone is not ready for landing");
            //Debug.WriteLine("issuing land command");
            droneControl.SendCommand(land);
        }

        void automateDrone_DoWork(object sender, DoWorkEventArgs e)
        {
#if USE_ALVAR
            //less than 20% battery and the drone will have an emergency state anyway...
            bool safeToFly =
                droneControl.NavigationData.batteryLevel > 20 &&
                false == droneControl.IsEmergency;
            
            while (true)
            {
                if (false == m_markerInRange)
                {
                    Debug.WriteLine("can't see marker");

                }
                else
                {
                    Debug.WriteLine("angley:{0}deg anglex:{1}deg distance:{2}units", 
                        m_yAxisAngleToMarkerDeg, m_xAxisAngleToMarkerDeg, m_distanceToMarker);

                    /*
                    if (m_yAxisAngleToMarkerDeg < 90)
                    {
                        Command up = new FlightMoveCommand(0, 0, 0, +0.1f);
                        Debug.Assert(droneControl.IsCommandPossible(up), "drone is not ready for movement");
                        droneControl.SendCommand(up);
                    }
                    else
                    {
                        Command down = new FlightMoveCommand(0, 0, 0, -0.1f);
                        Debug.Assert(droneControl.IsCommandPossible(down), "drone is not ready for movement");
                        droneControl.SendCommand(down);
                    }
                    */

                    if (m_distanceToMarker > 500)
                    {
                        Command fwd = new FlightMoveCommand(0, -0.1f, 0, 0);
                        Debug.Assert(droneControl.IsCommandPossible(fwd), "drone is not ready for movement");
                        droneControl.SendCommand(fwd);
                    }
                    else
                    {
                        Command bwd = new FlightMoveCommand(0, +0.1f, 0, 0);
                        Debug.Assert(droneControl.IsCommandPossible(bwd), "drone is not ready for movement");
                        droneControl.SendCommand(bwd);
                    }
                }
            }
#endif
        }

        void droneControl_NetworkConnectionStateChanged(object sender, DroneNetworkConnectionStateChangedEventArgs e)
        {
            if (e.State == DroneNetworkConnectionState.PingSuccesful)
            {
                droneControl.NetworkConnectionStateChanged -= new DroneNetworkConnectionStateChangedEventHandler(droneControl_NetworkConnectionStateChanged);
            
                //this event is dispatched by the dronecontrol.networkConnector
                //which means a function pointer call across thread contexts
                //so we poll against a flag as a naive solution
                m_isDroneBootstrapComplete = true;
            }
        }

        #endregion

        #region helpers

        private void Connect()
        {
            Debug.WriteLine("\t *** Connecting to the drone");
            droneControl.ConnectToDrone();
        }

        private void Disconnect()
        {
            //timerVideoUpdate.Stop();
            //timerStatusUpdate.Stop();

            //automation
            droneControl.SendCommand(
                new FlightModeCommand(DroneFlightMode.Land));
            
            Debug.WriteLine("\t *** Disconnecting from drone");
            droneControl.Disconnect();
        }

        private void InitializeDroneControl()
        {
            currentDroneConfig = new DroneConfig();
            //currentDroneConfig.Load();

            InitializeDroneControl(currentDroneConfig);
        }

        private void InitializeDroneControl(DroneConfig droneConfig)
        {
            droneControl = new DroneControl(droneConfig);
        }

        private void InitializeDroneControlEventHandlers()
        {
            droneControl.Error += droneControl_Error_Async;
            droneControl.ConnectionStateChanged += droneControl_ConnectionStateChanged_Async;
        }

        private void InitializeTimers()
        {
#if USE_WPF
            //timerStatusUpdate = new DispatcherTimer();
            //timerStatusUpdate.Interval = new TimeSpan(0, 0, 50);
            //timerStatusUpdate.Tick += new EventHandler(timerStatusUpdate_Tick);

            //timerVideoUpdate.Tick += new EventHandler(timerVideoUpdate_Tick);
#endif
            TimerCallback videoUpdateTimerCallback = this.getFrame;
            TimerCallback statusUpdateTimerCallback = this.getStatusUpdate;
            
            timerVideoUpdate = new Timer(videoUpdateTimerCallback,null,0,VIDEO_UPDATE_RATE);
            timerStatusUpdate = new Timer(statusUpdateTimerCallback,null,0,STATUS_UPDATE_RATE);   
        }

        private void getFrame(Object stateInfo)
        {
            //  Get a new image from the AR-Drone
            //  this also will fire an event
            SetNewVideoImage();
        }

        private void getStatusUpdate(Object stateInfo)
        {
            //  Get new status information from the
            //  drone and fire and event
            UpdateStatus();
        }

        private void HandleError(DroneErrorEventArgs args)
        {
            String errorText = SerializeException(args.CausingException);
            Debug.WriteLine(errorText);
        }

        private String SerializeException(Exception e)
        {
            String errorMessage = e.Message;
            String exceptionTypeText = e.GetType().ToString();
            String stackTrace = e.StackTrace == null ? "No stack trace given" : e.StackTrace.ToString();

            String errorText = "An exception '" + exceptionTypeText + "' occured:\n" + errorMessage;
            errorText += "\n\nStack trace:\n" + stackTrace;

            if (e.InnerException != null)
            {
                errorText += "\n\n";
                errorText += SerializeException(e.InnerException);
            }

            return errorText;
        }

        #endregion

        #region eventHandlers

        void droneControl_Error_Async(object sender, DroneErrorEventArgs e)
        {
            HandleError(e);
        }
        
        void droneControl_ConnectionStateChanged_Async(object sender, DroneConnectionStateChangedEventArgs e)
        {
            HandleConnectionStateChange(e);
        }
        
        private void HandleConnectionStateChange(DroneConnectionStateChangedEventArgs args)
        {
            if (args.Connected)
            {
                Debug.WriteLine("\t *** Connected to the drone");
                Debug.Assert(false == droneControl.IsEmergency, "something is wrong - drone is in emergency mode");

                m_isDroneConnectionComplete = true;
            }
            else
            {
                Debug.WriteLine("\t *** Disconnected from the drone");
            }
        }

        private void timerStatusUpdate_Tick(object sender, EventArgs e)
        {
            UpdateStatus();
        }

        private void UpdateStatus()
        {
            data = droneControl.NavigationData;
#if CONSOLE_OUTPUT            
            Debug.WriteLine("\tbattery :{0} altitude:{1}", 
                data.BatteryLevel.ToString() + "%", 
                data.Altitude.ToString());
#endif
            if (NewStatusUpdate != null)
            {
                NewStatusUpdate(this, EventArgs.Empty);
            }
        }

        void timerVideoUpdate_Tick(object sender, EventArgs e)
        {
            SetNewVideoImage();
        }

        private void SetNewVideoImage()
        {
            System.Drawing.Bitmap image = droneControl.BitmapImage;
            //System.Drawing.Bitmap image = new System.Drawing.Bitmap("MarkerImage.jpg");

            if (image == null)
            {
                return;
            }

            //expose the frame for the viewmodel
            m_lastFrameCaptured = image;

            if (NewFrameCaptured != null)
            {
                NewFrameCaptured(this, EventArgs.Empty);
            }

            //from videoCapture sample in arDrone
            int frameWidth  = droneControl.FrontCameraPictureSize.Width;
            int frameHeight = droneControl.FrontCameraPictureSize.Height;
            //string channelSeq;
            //int nChannels;
                
            //initialise the image properties -----------------------------------------------------------------------------------

            //wrapped *int for interop
            IntPtr imagePtr = IntPtr.Zero;
            IntPtr singleMarkerIDsPtr = IntPtr.Zero; //this is a multi tracker so we don't use this pointer

            try
            {
                //get pointers
                System.Drawing.Imaging.BitmapData data = image.LockBits(new System.Drawing.Rectangle(0, 0, frameWidth, frameHeight),
                    System.Drawing.Imaging.ImageLockMode.ReadOnly, image.PixelFormat);
                /*
                Debug.WriteLine("width:{0} height:{1} format:{2} stride:{3}",
                    data.Width, data.Height, data.PixelFormat, data.Stride);
                */
                //initialise the image properties
                imagePtr = data.Scan0;

                /*
                if (data.PixelFormat == System.Drawing.Imaging.PixelFormat.Format24bppRgb)
                {
                    channelSeq = "RGB";
                    nChannels = 3;
                }
                else if (data.PixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppRgb)
                {
                    channelSeq = "RGB";
                    nChannels = 4;
                }
                else
                {
                    throw new Exception("unexpected pixel format: " + data.PixelFormat.ToString());
                }
                */

                image.UnlockBits(data);
            }
            //  This exception will get thrown if the image is already locked
            catch (InvalidOperationException ex)
            {

                Console.WriteLine(ex.ToString());
            }

#if USE_ALVAR
            //initialise the marker properties-------------------------------------------------------------------
            int foundMarkerNums = 0;
            double max_marker_error = 0.02f; // Tutorial 8
            int interestedMarkerNums = 0;
            double max_track_error = 0.2; //alvarMarkerTracker default

            /* GW - this injects the parameters into an openCV IPLImage struct:
                * 
                *      http://opencv.willowgarage.com/documentation/basic_structures.html
                *      imagePtr points to the first pixelData in the buffer and manifests as char* in the wrapper
                * 
                * then wraps a call on to the marker detector to markerDetectors[i]->Detect 
                * the result is iterated: found markers are saved internally whilst the number of found markers is 
                * piped back out via reference:
                * 
                *      numinterested markers appears to be how many were searched for in the scene (state property of bridge)
                *      numfoundmarkernums appears to be how many interested markers were found
                */


            ALVARDllBridge.alvar_detect_marker(m_alvarDetectorID, m_alvarCameraID, nChannels, channelSeq, channelSeq,
                                                imagePtr, singleMarkerIDsPtr, ref foundMarkerNums, ref interestedMarkerNums,
                                                max_marker_error, max_track_error);
            
            m_markerInRange = true;
            if (foundMarkerNums == 0)
            {
                m_markerInRange = false;
                return;
            }

            //from GoblinXNA.Device.Vision.Marker.alvarMarkerTracker.cs
            bool detectAdditional = false;
            ALVARDllBridge.alvar_get_multi_marker_poses(m_alvarDetectorID, m_alvarCameraID, detectAdditional,
            multiIdPtr, multiPosePtr, multiErrorPtr);

            Marshal.Copy(multiIdPtr, multiIDs, 0, multiMarkerIDs.Count);
            Marshal.Copy(multiPosePtr, multiPoseMats, 0, multiMarkerIDs.Count * 16);
            Marshal.Copy(multiErrorPtr, multiErrors, 0, multiMarkerIDs.Count);

            /*
            for (int i = 0; i < multiMarkerIDs.Count; i++)
            {
                int id = multiIDs[i];
                double error = multiErrors[i];

                if (error == -1)
                    continue;

                int index = i * 16;
                
                Matrix3D poseTransform = new Matrix3D(
                    (float)multiPoseMats[index], (float)multiPoseMats[index + 1], (float)multiPoseMats[index + 2], (float)multiPoseMats[index + 3],
                    (float)multiPoseMats[index + 4], (float)multiPoseMats[index + 5], (float)multiPoseMats[index + 6], (float)multiPoseMats[index + 7],
                    (float)multiPoseMats[index + 8], (float)multiPoseMats[index + 9], (float)multiPoseMats[index + 10], (float)multiPoseMats[index + 11],
                    (float)multiPoseMats[index + 12], (float)multiPoseMats[index + 13], (float)multiPoseMats[index + 14], (float)multiPoseMats[index + 15]);

                //distance to marker
                Point3D poseVertex = new Point3D();
                poseVertex = Point3D.Multiply(poseVertex, poseTransform);

                m_distanceToMarker = Math.Sqrt(poseVertex.X * poseVertex.X +
                                                    poseVertex.Y * poseVertex.Y +
                                                    poseVertex.Z * poseVertex.Z);

                Vector3D up = new Vector3D(0, 1, 0);
                Vector3D toMarker = new Vector3D(poseVertex.X, poseVertex.Y, poseVertex.Z);
                m_yAxisAngleToMarkerDeg = Vector3D.AngleBetween(up, toMarker);

                Vector3D fwd = new Vector3D(0, 0, 1);
                m_xAxisAngleToMarkerDeg = Vector3D.AngleBetween(fwd, toMarker);

                //Debug.WriteLine("multi-marker: {0} angleTo: {1} deg distance: {2} units(mm at 96dpi default)", i, m_yAxisAngleToMarkerDeg, m_distanceToMarker);
 
            }
#endif            //* */
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            try
            {
                //according to the sdk, this command should be sent repeatedly until
                //the drone navdata registers that the drone is landing...
                Command land = new FlightModeCommand(DroneFlightMode.Land);
                //Debug.Assert(droneControl.IsCommandPossible(land), "drone is not ready for landing");
                Debug.WriteLine("issuing land command");
                droneControl.SendCommand(land);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            this.timerVideoUpdate.Dispose();

            this.timerStatusUpdate.Dispose();

            Disconnect();
        }

        /**
         *  Issue a command to the ARDrone to take off
         *  
         *  @author dean
         *  @date 06-03-2013
         */
        public void TakeOff()
        {
            Command takeOff = new FlightModeCommand(DroneFlightMode.TakeOff);
            //Debug.Assert(droneControl.IsCommandPossible(takeOff), "drone is not ready for landing");
            Debug.WriteLine("issuing take off command");
            droneControl.SendCommand(takeOff);
        }

        /**
         *  Issue a command to the ARDrone to land
         *  
         *  @author dean
         *  @date 06-03-2013
         */
        public void Land()
        {
            Command land = new FlightModeCommand(DroneFlightMode.Land);
            //Debug.Assert(droneControl.IsCommandPossible(land), "drone is not ready for landing");
            Debug.WriteLine("issuing land command");
            droneControl.SendCommand(land);
        }


        public void StrafeLeft()
        {
            //  move left and right by altering the roll axis
            move(0, 0, -0.1f, 0);
        }

        public void StrafeRight()
        {
            move(0, 0, 0.1f ,0);
        }

        private void move(float yaw, float pitch, float roll, float gaz)
        {
            //  Wiki page detailing Yaw, Pitch and Roll
            //  http://en.wikipedia.org/wiki/Aircraft_principal_axes

            Command move = new FlightMoveCommand(roll, pitch, yaw,gaz);
            //Debug.Assert(droneControl.IsCommandPossible(move), "drone is not ready for landing");
            Debug.WriteLine("issuing land command");
            droneControl.SendCommand(move);
        }

        public void stop()
        {
            move(0, 0, 0, 0);
        }

        public void Up()
        {
            move(0, 0, 0, 0.25f);
        }

        public void Down()
        {
            move(0, 0, 0, -0.25f);
        }

        public void MoveBackward()
        {
            move(0, 0.1f, 0, 0);
        }

        public void MoveForward()
        {
            move(0, -0.1f, 0, 0);
        }

        public void RotateClockwise()
        {
            move(0.25f, 0, 0, 0);
        }

        public void RotateAntiClockwise()
        {
            move(-0.25f, 0, 0, 0);
        }

        #endregion
    }
}
