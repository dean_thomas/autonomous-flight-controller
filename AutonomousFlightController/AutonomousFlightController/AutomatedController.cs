﻿//#define DISABLE_AUTONOMOUS_TAKEOFF

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using arDroneOpenCv.Model;
using System.Drawing;

/**
 *  @class AutonomousFlightController
 *  @author Dean Thomas
 *  @email dean.thomas@postgrad.plymouth.ac.uk
 *  
 *  This class issues flight commands:
 *      1.  Land / Take-off
 *      2.  Rotate and Move up / down
 *      3.  Move Forward / Back
 *      
 *  This is based upon:
 *      1.  The percentage of frames in which the target where visible in the
 *          last 3 seconds
 *      2.  The offset vector between field of view centre and the target
 *      3.  The size of the target area
 */
namespace AutonomousFlightController
{
    class AutomatedController
    {
        #region Constants

        //  Perecentage of frames where target should be
        //  visible to issue the relvant commands
        public const float LAND_THRESHOLD = 0.1f;
        public const float TAKE_OFF_THRESHOLD = 0.8f;

        //  Offer a small buffer in the movement in
        //  the up/down/left/right directions so that
        //  the drone doesnt instantly move and make
        //  movement judder
        public const int SIDEWAYS_OFFSET_THRESHOLD = 5;
        public const int UP_DOWN_OFFSET_THRESHOLD = 50;

        //  Using an enumerate, set the distance that the
        //  drone should follow behind the user
        private Follower.FollowDistance followDistance = Follower.FollowDistance.MEDIUM;

        #endregion Constants

        #region Variables

        //  Hold a reference to the drone interface
        private DroneVision droneVision;

        //  Can disable takeoff, if we want the drone to
        //  remain grounded - useful for testing targeting
        private Boolean disableTakeoff;
        public Boolean DisableTakeoff { get { return disableTakeoff; } set { disableTakeoff = value; } }

        #endregion Variables

        #region Main function

        /**
         *  Move the drone based upon the passed in parameters from the image processor
         */
        public void controlLoop(float percentageOfMatches,Size offsetVector,float contourBoxArea)
        {
            //  Two ways to disable takeoff:
            //  1.  through the user interface
            //  2.  hardcoded using pre-processor symbol
            #if !DISABLE_AUTONOMOUS_TAKEOFF
            if (!disableTakeoff)
            {
                //  If the target has been seen take off
                if (percentageOfMatches > TAKE_OFF_THRESHOLD)
                {
                    droneVision.TakeOff();
                }
                //  Else land
                else if (percentageOfMatches < LAND_THRESHOLD)
                {
                    droneVision.Land();
                }
            }
            #endif
            
            //  Move the drone forward if the size is below the target size or
            //  back if the drone if too close
            if (contourBoxArea < Follower.targetPixelCountForFollowDistance(followDistance))
            {
                droneVision.MoveForward();
            }
            else if (contourBoxArea > Follower.targetPixelCountForFollowDistance(followDistance))
            {
                droneVision.MoveBackward();
            }

            //  Rotate to centre the tracked object
            if (offsetVector.Width != int.MaxValue)
            {
                if (offsetVector.Width > SIDEWAYS_OFFSET_THRESHOLD)
                {
                    droneVision.RotateAntiClockwise();
                }
                else if (offsetVector.Width < -SIDEWAYS_OFFSET_THRESHOLD)
                {
                    droneVision.RotateClockwise();
                }
            }

            //  Move up and down to centre the tracked object
            if (offsetVector.Height != int.MaxValue)
            {
                //Console.WriteLine("Vertical offset: " + offsetVector.Height.ToString());

                if (offsetVector.Height > UP_DOWN_OFFSET_THRESHOLD)
                {
                    droneVision.Up();
                }
                else if (offsetVector.Height < -UP_DOWN_OFFSET_THRESHOLD)
                {
                    droneVision.Down();
                }
            }
        }

        #endregion main function

        #region Constructor

        /**
         *  Create the control object to interact with the
         *  provided drone interface
         */
        public AutomatedController(DroneVision droneVision)
        {
            // TODO: Complete member initialization
            this.droneVision = droneVision;
            this.followDistance = Follower.FollowDistance.MEDIUM;
        }

        #endregion constructor
    }
}
