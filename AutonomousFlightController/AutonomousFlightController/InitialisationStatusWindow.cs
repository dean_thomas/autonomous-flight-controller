﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using arDroneOpenCv.Model;

namespace AutonomousFlightController
{
    public partial class InitialisationStatusWindow : Form
    {
        DroneVision droneVision;

        public InitialisationStatusWindow()
        {
            InitializeComponent();
        }

        public InitialisationStatusWindow(DroneVision droneVision)
            : this()
        {
            this.droneVision = droneVision;

            droneVision.InitialisationStatusChangeEvent += new EventHandler(droneVision_InitialisationStatusChange);
        }

        void droneVision_InitialisationStatusChange(object sender, EventArgs e)
        {
            progressBar1.PerformStep();

            if (droneVision.InitialisationStatus == DroneInitialisationStatus.COMPLETED)
            {
                this.Close();

                droneVision.InitialisationStatusChangeEvent -= new EventHandler(droneVision_InitialisationStatusChange);
            }
        }

        public void Increment(int value)
        {
            this.progressBar1.Increment(value);
        }
    }
}
