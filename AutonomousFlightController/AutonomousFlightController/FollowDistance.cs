﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutonomousFlightController
{
    static class Follower
    {
        /**
         *  Distances obtained by measuring contour areas at
         *  different distances
         */
        public const int TARGET_FAR = 900;
        public const int TARGET_MEDIUM = 13050;
        public const int TARGET_NEAR = 36500;

        /**
         *  Return the correct pixel count for the target distance
         */
        static public int targetPixelCountForFollowDistance(FollowDistance followDistance)
        {
            switch (followDistance)
            {
                case FollowDistance.FAR: return TARGET_FAR;
                case FollowDistance.MEDIUM: return TARGET_MEDIUM;
                case FollowDistance.NEAR: return TARGET_NEAR;
                default: return 0;
            }
        }

        public enum FollowDistance
        {
            NEAR = 0x00,
            MEDIUM = 0x01,
            FAR = 0x02
        }
    }
}
